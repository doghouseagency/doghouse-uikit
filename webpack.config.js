//const webpack = require('webpack');
const path = require('path');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const mode = 'production';

module.exports = [];

// Build CSS and style guide.
module.exports.push({
  mode,

  output: {
    filename: '[name].bundle.js',
    chunkFilename: '[name].[chunkhash].chunk.js',
    path: path.resolve(__dirname, 'css'),
    publicPath: "/css"
  },

  resolve: {
    extensions: ['.css', '.scss'],
    alias: {
      '~': path.resolve(process.cwd(), 'src'),
    },
  },

  entry: {
    "duk": './scss/duk-styles.scss',
    "styleguide": './scss/styleguide-only/styleguide.scss',
  },

  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          { loader: "kss-loader" },
          MiniCssExtractPlugin.loader,
          { loader: "css-loader", options: { url: false, importLoaders: 1 } },
          { loader: 'postcss-loader', options: { plugins: [autoprefixer(), cssnano()] }},
          { loader: 'sass-loader' },

        ],
      },
    ],
  },

  plugins: [
    // Assign a non-generic name to the stylesheet.
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),

    // Copy built assets to style guide.
    // Both copies below result in duplicate content in the repo but is the easiest solution for now.
    // Copy output files to the style guide.
    new CopyWebpackPlugin([{
      from: path.resolve(process.cwd(), 'css/*.css'),
      to: path.resolve(process.cwd(), 'styleguide/')
    }], {}),
    // Copy icon fonts into style guide.
    new CopyWebpackPlugin([{
      from: path.resolve(process.cwd(), 'icons/*'),
      to: path.resolve(process.cwd(), 'styleguide/')
    }], {})
  ]
});
