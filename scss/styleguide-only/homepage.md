# Doghouse UiKit Style guide

This style guide is built dynamically using only the comments in the scss files of the UiKit. Any changes to the scss 
should also follow the same conventions to ensure everything is documented in this style guide.

The format for comments is [KSS](https://warpspire.com/kss/) and documentation/syntax can be found 
[here](https://warpspire.com/kss/syntax/)

## Additions to the UiKit

Any additions to this UiKit should provide examples of usage. This both validates that what you have done works and 
provides documentation and inspiration for the next person.

## Building the style guide

This style guide is built using [KSS-Node](https://github.com/kss-node/kss-node) which can be executed manually or 
via task runners such as [webpack](https://www.npmjs.com/package/kss-webpack-plugin) which is the preferable method 
as you can build your CSS at the same time.

## All projects should have a style guide

This is full of examples of how you can add a living style guide to your next project. Browse the code for usage 
examples.


