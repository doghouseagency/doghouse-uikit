// Typography
//
// Typography related tools.
// -------------------------------------------------------

// All headings and their name.
$duk-headings: (
  h1: 'alpha',
  h2: 'beta',
  h3: 'gamma',
  h4: 'delta',
  h5: 'epsilon',
  h6: 'zeta',
) !default;

// All named font sizes sizes.
$duk-font-size-list: ('x-small', 'small', 'regular', 'large', 'x-large') !default;

// duk-headings
//
// Apply styles to all headings.
//
// Style guide: Mixins.duk-headings
@mixin duk-headings() {
  @each $tag, $name in $duk-headings {
    #{$tag}, .#{$duk-namespace}#{$name} {
      @content;
    }
  }
}

// duk-heading
//
// Apply style to a single heading tag.
//
// $tag - The heading tag to apply the style to
//
// Style guide: Mixins.duk-heading
@mixin duk-heading($tag) {
  $name: map-get($duk-headings, $tag);
  #{$tag}, .#{$duk-namespace}#{$name} {
    margin: $duk-heading-margin;
    @content;
  }
}

// duk-font-size
//
// Set a font size and line height from $duk-base-font-sizes.
//
// $size - The key that represents the size in the sizes map.
// $font-sizes - A map of the font sizes. Each value contains a map with `size` and `height`.
//
// Style guide: Mixins.duk-font-size
@mixin duk-font-size($size, $font-sizes: $duk-base-font-sizes) {
  font-size: map-deep-get($font-sizes, $size, 'size');
  line-height: map-deep-get($font-sizes, $size, 'height');
}

// duk-font-weight
//
// Set a font weight from `$duk-base-font-weights`
//
// $weight - The key that represents the font weight.
// $font-weights - A map of the font weights.
//
// Style guide: Mixins.duk-font-weight
@mixin duk-font-weight($weight, $font-weights: $duk-base-font-weights) {
  font-weight: map-get($font-weights, $weight);
}

// duk-get-font-color
//
// Get a font color from $duk-base-font-sizes.
//
// $color-name - The key that represents the color in the color map.
// $font-colors - A map of the font colors.
//
// Style guide: Functions.duk-get-font-color
@function duk-get-font-color($color-name, $font-colors: $duk-font-colors) {
  @return map-get($font-colors, $color-name);
}

// duk-font-color
//
// Set a font color from $duk-base-font-sizes.
//
// $color-name - The key that represents the color in the color map.
// $important - Should be !important true/false (defaults to false).
// $font-colors - A map of the font colors.
//
// Style guide: Mixins.duk-font-color
@mixin duk-font-color($color-name, $important: false, $font-colors: $duk-font-colors) {
  @if $important {
    color: duk-get-font-color($color-name, $font-colors) !important;
  }
  @else {
    color: duk-get-font-color($color-name, $font-colors);
  }
}

// duk-font-load
//
// Include a font face.
//
// $name - The name of the font family.
// $path - Path of the font folder.
// $filename - Filename for the font, expects 3 files with extensions: eot, woff, ttf.
// $weight - Weight of the font face (default normal).
// $style - Style of the font. Eg normal, italic (default normal).
// $formats - A map of font extensions and their format value.
//
// Style guide: Mixins.duk-font-load
@mixin duk-font-load($name, $path, $filename, $weight: normal, $style: normal, $formats: $duk-font-load-formats) {
  $src: null;
  $eot: false;

  @each $ext in $formats {
    // Use extension as format if not format is specified.
    $format: if(map-has-key($duk-font-format-map, $ext), map-get($duk-font-format-map, $ext), $ext);

    // Add IE6-IE8 fix if eot extension is present.
    @if $ext == 'eot' {
      $ext: 'eot?#iefix';
      $eot: true;
    }
    $src: append($src, url("#{$path}/#{$filename}." + $ext) format("#{$format}"), comma);
  }

  @font-face {
    font-family: $name;
    @if $eot {
      // IE9 Compat Mode.
      src: url("#{$path}/#{$filename}.eot");
    }
    src: $src;
    font-weight: $weight;
    font-style: $style;
  }
}

// duk-font-include($var)
//
// Import an external font or load a map of local font-face variants.
//
// ``` scss
// $font-variants: (
//   'tt_commons': (
//     path : '../../fonts/tt_commons',
//     formats: ('woff', 'woff2', 'eot'),
//     variants: (
//       ('tt_commons_regular-webfont', 400, normal),
//       ('tt_commons_medium-webfont', 500, normal),
//       ('tt_commons_bold-webfont', 700, normal),
//     ),
//   ),
// );
//
// @include duk-font-include('https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@400;600;700;900&display=swap');
// @include duk-font-include($font-variants)
// ```
//
// $var - An external resource to import (eg: google font).
// <br> A map of font variants.
//
// Style guide: Mixins.duk-font-include
@mixin duk-font-include($var) {
  @if type-of($var) == 'map' {
    @each $family, $options in $var {
      $path: map-get($options, path);
      $formats: map-get($options, formats);

      @each $variant in map-get($options, variants) {
        $filename: nth($variant, 1);
        $weight: nth($variant, 2);
        $style: nth($variant, 3);
        @include duk-font-load($family, $path, $filename, $weight, $style, $formats);
      }
    }
  }
  @if type-of($var) == 'string' {
    @import url($var);
  }
}

// duk-hr
//
// Style a `<hr>` element.
//
// $color - The color of the hr.
//
// Style guide: Mixins.duk-hr
@mixin duk-hr($color: rgba(0,0,0,0.1), $gutter: $duk-base-gutter) {
  margin-top: $gutter;
  margin-bottom: $gutter;
  border: 0;
  border-top: 1px solid $color;
}

// duk-blockquote
//
// Block quote text helper. Provides a nice default style for block quotes.
//
// $color-key - Text and border color. see `$duk-font-colors`
// $gutter - Vertical margin and left padding applied.
//
// Style guide: Mixins.duk-blockquote
@mixin duk-blockquote($color-key: 'blockquote', $gutter: ($duk-base-gutter * 2)) {
  $color: duk-get-font-color($color-key);
  color: $color;
  padding: 0 $gutter;
  margin: $gutter 0;
  border: 0;
  font-style: italic;
  font-size: 1.1em;
  border-left: 5px $color solid;
}

// duk-sr-only
//
// Hide an element but still make it available to screen readers.
//
// Style guide: Mixins.duk-sr-only
@mixin duk-sr-only {
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  overflow: hidden;
  clip: rect(0,0,0,0);
  white-space: nowrap;
  border: 0;
}

// duk-text-crop
//
// Ensure text remains on one line and overflow text is cropped with ellipsis (...).
//
// Stlye guide: Mixins.duk-text-crop
@mixin duk-text-crop() {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
