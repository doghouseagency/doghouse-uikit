# Doghouse UI Kit v2.x

The [Doghouse](https://doghouse.agency) UiKit is a SASS framework consisting of many tools and styles to make styling 
websites easier and more consistent. It tries not to impose too much, more just providing some sane defaults and lots 
of mixins that assist in common tasks.

Version `2.x` of UiKit is intended to compile **without** Compass or Ruby, so it can be compiled with 
[Node Sass](https://www.npmjs.com/package/node-sass) which is much faster and a lot easier to setup dev environments. 
The preferred install method for version 2 is via `npm`.

Version `1.x` requires Ruby/Compass and is generally installed with composer or git sub-module.

## Installation

You can add the UiKit to your project via 3 methods.

### NPM (recommended)

Usage instructions assume you use this method.
```
npm install --save @doghouse/uikit 
```

### Composer (legacy)

Add doghouse packages repo (if not already in your `composer.json`)
```
composer config repositories.doghouse composer https://packages.doghouse.agency/
```
Add the UiKit package
```
composer require doghouse/uikit:${version}
```

Where `${version}` is the git tag.

### Git clone / download (not recommended)

Simply clone or download this repo into your project.

## Dependencies

Dependencies should be as light as possible as this is reused many time.

* [Breakpoint Saas](https://www.npmjs.com/package/breakpoint-sass) - Assist with media queries.

## Usage

The easiest way to get started for a new project is to copy the [boilerplate](boilerplate) dir into your 
`mytheme/styles/scss` folder and start tweaking to your needs. The boilerplate contains an ideal folder structure 
and some sample files that include some KSS comments to encourage you to continue through development.

Otherwise, simply import `duk` into your theme, optionally toggle if base styles are included (recommended) and 
ensure dependencies are added beforehand. Eg:

```
// Requires breakpoint-sass
@import "~breakpoint-sass/stylesheets/breakpoint";

// Set this to false if you do not want styles included (ie. just use mixins and functions).
$duk-include-styles: true;

// Override/extend any other settings in "scss/_settings.scss"

// Import duk.
@import "~@doghouse/duk/scss/duk";
```

The above assumes you are using `webpack` to build and the `~` will automatically expand to `node_modules/` path.

### Size

You can just include UiKit settings and tools into your SASS/theme and it will add no markup (0KB). You can then just 
use the mixins as required.

The **recommended approach** is to include the base generated UiKit styles (~50KB minified) as it provides an excellent 
starting point for new projects with sane resets and defaults. See the styleguide for what you get.

To toggle the inclusion of the base generated styles, set `$duk-include-styles` to `true` or `false`.

### Settings as SASS Maps

Almost all settings are in SASS maps so the can be easily merged and iterated over to dynamically create modifiers and 
styles. 

`map-merge` is used to extend the defaults. This means defining or changing settings will not remove other defaults in
the same map.

Eg `scss/_settings.scss` uses a lot of this:

```
$duk-base-some-settings: () !default;
$duk-base-some-settings: map-merge(( ... ), $duk-base-some-settings);
```

More specific example, if you wanted a new button modifier `.button--fulro`, you would just add the following to your 
settings (that get included before `@import "duk")`:

```
$duk-button-colors: ( 'fulro':  #f42534 );
```

And your your new button class would be automatically created. The same is true for most of the variables/maps in the 
`settings.scss`.

### Compiling SCSS into SASS for your own project

Highly recommend you use the [Doghouse webpack project](https://github.com/DoghouseMedia/webpack-project) to get you 
started. 

For a more lightweight CSS only solution, see `package.json` and `webpack.config.js` in the root of this repo.

## Style guide

This UiKit produces a living style guide via the [KSS Syntax](https://warpspire.com/kss/) so you can easily browse 
the tools available. The compiled style guide builds to the `styleguide` directory in the root of this repo.

#### [View the style guide](https://uikit.doghouse.agency)

Or just read the [Readme](scss/styleguide-only/homepage.md).

### Rebuilding the styleguide

You can rebuild the style guide with 2 commands as configuration is stored in [kss-config.json](kss-config.json).

```
npm install
npm run build
```

## Icons

Read [this readme](icons/README.md) about using icons and icon helpers. Due to path inclusion issues, you will probably 
need to update `$duk-icon-font-path` to resolve correctly to a public path. This may be used in conjunction with a 
webpack helper.

Something like:
```
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  ...
  plugins: [
      new CopyWebpackPlugin([{
        from: path.resolve(process.cwd(), 'node_modules/@doghouse/uikit/icons/*'),
        to: path.resolve(process.cwd(), 'docroot/themes/custom/my_theme')
      }], {})
  ]
}
```

TODO: Validate above or come up with a better solution.

## Todo/Issues

* Due to execution order you need to run `npm run build` twice to get the css into the style guide.
* Icon fonts and their public location. Applies to style guide too.
* Improve organisation of style guide
* Add variable reference to style guide eg colors, etc.

## Troubleshooting

#### If you get the error relating to math.div when running `npm run build`

This is caused by not using 'dart-sass' (package `sass`). You are probably using 'node-sass' instead.
To fix this run the following on your project:

```shell
npm uninstall node-sass --save && npm install sass --save-dev
```

## Notice

[Node sass](https://www.npmjs.com/package/node-sass) package is deprecated and should be removed from your project.
It is replaced with [sass](https://www.npmjs.com/package/sass) which is a JS only implementation of SASS. 

## Contribute

Help make this even better, fix some code, add a feature and do a PR today! If you note an issue while working on a 
project, don't simply workaround in your own code, feed it back upstream into this package.

[Source repository](https://bitbucket.org/doghouseagency/doghouse-uikit/src/2.x/)

## Authors

[Sponsored by Doghouse Agency](https://doghouse.agency)

Original UiKit created by [Jorege Castro](https://github.com/jorgegc/). Version 2.x rebuild by 
[Jeremy Graham](https://github.com/jez500/). With loads of contributions from Doghouse staff along the way.
